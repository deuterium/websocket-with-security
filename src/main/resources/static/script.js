
var stompClient = null;

function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/atom/test', function (atom) {
            console.log("Inside function");
            console.log("Atom: " + atom);
        });
    });
}

function sendMessage() {
    stompClient.send("/app/test",{},"HI");
}
