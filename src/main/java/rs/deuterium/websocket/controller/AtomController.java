package rs.deuterium.websocket.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.deuterium.websocket.model.Atom;

/**
 * Created by MilanNuke on 9/13/2018
 */

@RestController
@RequestMapping("/atom")
@EnableScheduling
public class AtomController {

    private Logger log = LoggerFactory.getLogger(AtomController.class);

    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping("/test")
    public void test(){
        Atom atom = new Atom("Hydrogen", 1);
        log.info("Sending atom: " + atom + " to /atom/test");
        template.convertAndSend("/atom/test", atom);
    }

//    @Scheduled(fixedRate = 10000)
    public void test02(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication.isAuthenticated()){
            String currentPrincipalName = authentication.getName();
            String message = "Hello, " + currentPrincipalName;
            template.convertAndSendToUser(currentPrincipalName, "/atom/test", message);
        }
    }


}
