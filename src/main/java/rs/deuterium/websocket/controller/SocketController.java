package rs.deuterium.websocket.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

/**
 * Created by MilanNuke on 9/13/2018
 */
@Controller
public class SocketController {

    private Logger log = LoggerFactory.getLogger(AtomController.class);

    @Autowired
    private SimpMessagingTemplate template;

    // Receive client messages to destination "/milan"
    // Process + send to subscribers of "/atom/test"
    @MessageMapping("/milan")
    @SendTo("/atom/test")
    public String test(String message) {

        return "Hello, " + message;
    }

    @MessageMapping("/test")
    public void test02(String mess, Authentication authentication){
        log.info("Received: " + mess);
        if(authentication.isAuthenticated()){
            String currentPrincipalName = authentication.getName();
            String message = "Hello, " + currentPrincipalName;
            template.convertAndSendToUser(currentPrincipalName, "/atom/test", message);
            log.info("Message sent to " + currentPrincipalName);
        }
    }

    @SubscribeMapping("/subscribe")
    public void test03(Authentication authentication){
        if(authentication.isAuthenticated()){
            String currentPrincipalName = authentication.getName();
            template.convertAndSendToUser(currentPrincipalName, "/atom/test", "You subscribed");
            log.info("Subscribed: " + currentPrincipalName);
        }
    }

}
