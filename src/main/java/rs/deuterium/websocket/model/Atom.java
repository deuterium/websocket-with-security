package rs.deuterium.websocket.model;

/**
 * Created by MilanNuke on 9/13/2018
 */
public class Atom {
    private String name;
    private Integer ordNumber;

    public Atom() {
    }

    public Atom(String name, Integer ordNumber) {
        this.name = name;
        this.ordNumber = ordNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrdNumber() {
        return ordNumber;
    }

    public void setOrdNumber(Integer ordNumber) {
        this.ordNumber = ordNumber;
    }

    @Override
    public String toString() {
        return "Atom{" +
                "name='" + name + '\'' +
                ", ordNumber=" + ordNumber +
                '}';
    }
}
